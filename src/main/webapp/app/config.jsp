<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<title>APP_DISPLAY_NAME_SET_BY_BUILD vVERSION_SET_BY_BUILD Settings</title>
	<script type="text/javascript" src="scripts/jquery/jquery-1.11.0.min.js"></script>
	<script type="text/javascript">
		(function() {
			jive.tile.onOpen(function(config, options ) {
				gadgets.window.adjustHeight();

				var oauthURL = "SERVICE_URL_SET_BY_BUILD/v1/jive/instance/oauth_url?_nc=" + Math.random();

				var getUrlRequest = osapi.http.get({href:oauthURL, format: "html"})
				getUrlRequest.execute(function(response) {
					var url = response.content;

					var popup = jive.tile.openOAuthPopup(
							url,
							"height=400, width=1000",
							function(){
								console.log("opened");
							},
							function(){
								console.log("closed");
							}
					);
					$("#register-link")[0].onclick = popup.createOpenerOnClick();
					$("#register-link").show();
				});

				$("#register-link").click( function() {
					jive.tile.close({}, config );
					gadgets.window.adjustHeight(400);
				});
			});
		})();
	</script>
</head>
<body>

Before this add-on can be used, it must be granted permission
to access the Jive REST API.  Please click the link below in order
to grant access to the add-on.
<a id="register-link" href="javascript:void(0)" target="_blank" style="display: none">Request Authorization</a>

</body>
</html>