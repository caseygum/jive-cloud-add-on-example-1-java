<!DOCTYPE html>
<html data-ng-app="theApp">
	<head>
		<title>APP_DISPLAY_NAME_SET_BY_BUILD (vVERSION_SET_BY_BUILD)</title>
		<script type="text/javascript" src="scripts/jquery/jquery-1.11.0.min.js"></script>
	</head>
	<body>
	<script type="text/javascript">
		jiveUrl = opensocial.getEnvironment().jiveUrl;

		if (window === top) {
			/*
			 If the user right-clicked a link in the app and selected open in new tab,
			 this will detect that condition and redirect accordingly

				var viewParams = null;
				if (window.location.hash && window.location.hash.length > 0) {
					viewParams = {"path": window.location.hash.substr(1)};
				}
				var redirectUrl = (viewParams === null) ?
						jiveUrl + "/apps/APP_PATH_SET_BY_BUILD" :
						jiveUrl + "/apps/APP_PATH_SET_BY_BUILD#canvas:" + JSON.stringify(viewParams);

				window.location.href = redirectUrl;
		   */
		}

		if (gadgets && gadgets.window && gadgets.window.setTitle) {
			gadgets.window.setTitle("APP_DISPLAY_NAME_SET_BY_BUILD (vVERSION_SET_BY_BUILD)");
		}

		var jiveBaseCss = jiveUrl + "/styles/jive-base.css";
		var jiveCss = jiveUrl + "/styles/jive.css";
		var jiveIconsCss = jiveUrl + "/styles/jive-icons.css";
		$("head").append('<link rel="stylesheet" type="text/css" media="all" href="' + jiveBaseCss + '"/>')
		$("head").append('<link rel="stylesheet" type="text/css" media="all" href="' + jiveIconsCss + '"/>')
		$("head").append('<link rel="stylesheet" type="text/css" media="all" href="' + jiveCss + '"/>')
	</script>

		<script type="text/javascript">

			var appConfig = {
				version: "VERSION_SET_BY_BUILD",
				baseDir: "SERVICE_URL_SET_BY_BUILD",
				appDir: "APP_URL_SET_BY_BUILD",
				viewsDir: appDir + "/views"
			}

			console.log("Loading APP_DISPLAY_NAME_SET_BY_BUILD vVERSION_SET_BY_BUILD");
			//var theApp = TODO - create new AngularJS app
		</script>

		My app is so cool.

	</body>
</html>