package com.ndriven.jive.cloud.examples.addon.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by caseygum on 2/10/14.
 */
public class LoggingFilter implements Filter {

	final Logger logger = LoggerFactory.getLogger(LoggingFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		logger.debug(((HttpServletRequest) servletRequest).getRequestURL().toString());
		filterChain.doFilter(servletRequest, servletResponse);
	}

	@Override
	public void destroy() { }
}
