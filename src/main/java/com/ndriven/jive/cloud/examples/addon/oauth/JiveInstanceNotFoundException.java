package com.ndriven.jive.cloud.examples.addon.oauth;

/**
 * Created by caseygum on 2/15/14.
 */
public class JiveInstanceNotFoundException extends Exception {
	private final static String ERROR_MESSAGE =  "JiveInstanceHolder contains a null or invalid JiveInstance.  Try going to the add-ons page and Reconnect to service.";
	public JiveInstanceNotFoundException() {
		super(ERROR_MESSAGE);
	}

	public JiveInstanceNotFoundException(Throwable cause) {
		super(ERROR_MESSAGE, cause);
	}
}
