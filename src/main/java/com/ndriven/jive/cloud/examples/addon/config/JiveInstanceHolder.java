package com.ndriven.jive.cloud.examples.addon.config;

import com.ndriven.jive.cloud.examples.addon.model.JiveInstance;
import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.ndriven.jive.cloud.examples.addon.util.EncryptionUtil.*;

import java.io.File;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;

/**
 * Created by caseygum on 2/12/14.
 */
public class JiveInstanceHolder {
	final Logger logger = LoggerFactory.getLogger(JiveInstanceHolder.class);
	private JiveInstance jiveInstance;

	private String encryptionKey;
    private String configDir;

	private String INSTANCE_DATA_FILE_NAME="jiveInstance.dat";

    public JiveInstanceHolder(String configDir, String encryptionKey) {
        this.configDir = configDir;
        this.encryptionKey = encryptionKey;
    }

    public JiveInstance getJiveInstance() throws IOException, GeneralSecurityException {
		if (jiveInstance == null) {
			try {
				jiveInstance = loadInstanceData();
			} catch (Exception e) {
				logger.error("An error occurred loading JiveInstance data", e);
				throw new RuntimeException(e);
			}
		}
		return jiveInstance;
	}

	public void setJiveInstance(JiveInstance jiveInstance) throws IOException, GeneralSecurityException {
		if (jiveInstance == null) {
			deleteInstanceData();
		}
		this.jiveInstance = jiveInstance;
		if (this.jiveInstance != null) {
			saveInstanceData();
		}
	}

	public void saveInstanceData() throws IOException, GeneralSecurityException {
		try {
			if (jiveInstance == null) throw new InvalidObjectException("jiveInstance cannot be null");
			String encodedData = encryptInstanceData(jiveInstance);
			File saveFile = getInstanceDataFile();
			PrintWriter out = new PrintWriter(saveFile);
			out.write(encodedData);
			out.flush();
			out.close();
		} catch (Exception ex) {
			logger.error("An error occurred saving Jive instance data", ex);
			throw new RuntimeException(ex);
		}
	}

	public String getConfigDir() {
		return configDir;
	}

	public void setConfigDir(String configDir) {
		this.configDir = configDir;
	}

	public void setEncryptionKey(String encryptionKey) {
		this.encryptionKey = encryptionKey;
	}

	private void deleteInstanceData() {
		File instanceDataFile = getInstanceDataFile();
		if (instanceDataFile.exists()) instanceDataFile.delete();
	}

	private JiveInstance loadInstanceData() throws IOException, GeneralSecurityException {
		File instanceFile = getInstanceDataFile();
		if (!instanceFile.exists()) return null;
		byte[] encoded = FileUtils.readFileToByteArray(instanceFile);
		String encodedEncryptedInstanceString = Charset.defaultCharset().decode(ByteBuffer.wrap(encoded)).toString();
		String decryptedInstanceString = base64DecodeAndDecrypt(encryptionKey, encodedEncryptedInstanceString);
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(decryptedInstanceString, JiveInstance.class);
	}

	private File getInstanceDataFile() {
        File dir = new File(getConfigDir());
        if (!dir.exists()) {
            try {
                Boolean success = dir.mkdirs();
                if (!success) throw new RuntimeException("Could not create directory");
            } catch (Exception ex) {
                logger.error("Error creating config directory", ex);
            }
        }
		return new File(getConfigDir(), INSTANCE_DATA_FILE_NAME);
	}

	private String encryptInstanceData(JiveInstance instance) throws IOException, GeneralSecurityException {
		ObjectMapper mapper = new ObjectMapper();
		String instanceAsJson = mapper.writeValueAsString(instance);
		return encryptAndBase64Encode(encryptionKey, instanceAsJson);
	}

	private JiveInstance decryptInstanceData(String encryptedData) {
		return null;
	}

}
