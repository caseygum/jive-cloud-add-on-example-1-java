package com.ndriven.jive.cloud.examples.addon.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 * Created by caseygum on 3/5/14.
 */

@Path("/jive/webhooks")
public class JiveWebhooksResource extends ResourceBase {
	final Logger logger = LoggerFactory.getLogger(JiveWebhooksResource.class);

	@POST
	public void webhookCallback(String body) {

        logger.info(body);

		setResponseStatus(HttpStatus.NO_CONTENT);
	}

}
