package com.ndriven.jive.cloud.examples.addon.rest;

import com.google.common.net.HttpHeaders;
import com.ndriven.jive.cloud.examples.addon.config.JiveInstanceHolder;
import com.ndriven.jive.cloud.examples.addon.model.JiveInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

/**
 * Created by caseygum on 2/10/14.
 */
public abstract class ResourceBase {
    @Autowired
	private JiveInstanceHolder jiveInstanceHolder;
	private JiveInstance jiveInstance;

    @Autowired
    @Qualifier("diagnosticsKey")
    private String diagnosticsKey;

    @Autowired
    @Qualifier("serviceUrl")
    private String serviceUrl;

	@Context private UriInfo context;
	@Context private HttpServletRequest request;
	@Context private HttpServletResponse response;

    protected void validateRequest() {
        String jiveAuthHeader = getRequest().getHeader(HttpHeaders.AUTHORIZATION);
        try {
            if (diagnosticsKey != null && diagnosticsKey.equals(getRequest().getHeader("X-Diagnostics-Key"))) {
                return;
            }
            if (getJiveInstance().isValidJiveAuthHeader(jiveAuthHeader)) {
                return;
            }
        } catch (Exception e) {
            throw new NotAuthorizedException("The Authorization header in the request could not be validated.", e, "JiveEXTN", null);
        }
        throw new NotAuthorizedException("The Authorization header in the request could not be validated.", "JiveEXTN", (Object[]) null);
    }

	protected Long getCurrentUserId() {
        return Long.parseLong(getRequest().getHeader("X-Jive-User-ID"));
	}

	protected UriInfo getContext() {
		return context;
	}

	protected HttpServletRequest getRequest() {
		return request;
	}

	protected HttpServletResponse getResponse() {
		return response;
	}

	protected void setResponseHeader(String headerName, String headerValue) {
		response.setHeader(headerName, headerValue);
	}

	protected void setResponseStatus(HttpStatus status) {
		setResponseStatus(status.value());
	}

	protected void setResponseStatus(int statusCode) {
		response.setStatus(statusCode);
	}

	protected void addCrossDomainHeaders() {
		response.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, getAccessControlAllowOriginUrls());
	}

	private String getAccessControlAllowOriginUrls() {
		String jiveUrl = getJiveInstance().getJiveUrl();
		StringBuffer sb = new StringBuffer();
		sb.append("*");

		//sb.append(jiveUrl);
		// TODO - add additional ones here (like https://site-app.jiveon.com)
		return sb.toString();
	}


	private JiveInstance getJiveInstance() {
		if (jiveInstance != null) return jiveInstance;
		try {
			jiveInstance = jiveInstanceHolder.getJiveInstance();
		} catch (Exception ex) {
		}

		return jiveInstance;
	}
}
