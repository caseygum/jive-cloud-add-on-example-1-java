package com.ndriven.jive.cloud.examples.addon.rest.exceptions;

import com.ndriven.jive.cloud.examples.addon.oauth.OAuthCodeNotFoundException;
import org.springframework.http.HttpStatus;

/**
 * Created by caseygum on 2/19/14.
 */
public class OAuthCodeNotFoundExceptionMapper extends ExceptionMapperBase<OAuthCodeNotFoundException> {
	@Override
	protected String getMessage() {
		return "An OAuth code has not been set on the Jive Instance object.  Jive needs to grant " +
			   "permission to this service in order for it to make use of Jive's REST API.  Requesting " +
			   "permission can be done from the configuration section of the App Add-on settings page in Jive.";
	}

	@Override
	protected int getStatusCode() {
		return HttpStatus.PRECONDITION_FAILED.value();
	}
}
