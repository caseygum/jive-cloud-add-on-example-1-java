package com.ndriven.jive.cloud.examples.addon.rest.serialization;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import javax.ws.rs.ext.Provider;

/**
 * Created by caseygum on 2/19/14.
 *
 * This just gets the JacksonJsonProvider into the namespace so that it can be picked up
 * without explicit configuration.
 *
 * The JacksonJsonProvider uses the Jackson JSON ObjectMapper to serialize JSON objects rather
 * than the built-in JAXB one, which always adds a "type" property onto the emitted JSON.
 *
 */
@Provider
@javax.ws.rs.Consumes({"application/json", "text/json"})
@javax.ws.rs.Produces({"application/json", "text/json"})
public class JacksonWriter extends JacksonJsonProvider {
}
