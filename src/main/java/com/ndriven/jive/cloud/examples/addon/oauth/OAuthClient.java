package com.ndriven.jive.cloud.examples.addon.oauth;

import javax.ws.rs.client.Invocation;

/**
 * Created by caseygum on 2/15/14.
 */
public interface OAuthClient {

	String getAccessToken() throws JiveInstanceNotFoundException, OAuthCodeNotFoundException;

	Invocation.Builder createRequestBuilder(String url) throws OAuthCodeNotFoundException, JiveInstanceNotFoundException;
	Invocation.Builder createMultipartRequestBuilder(String url) throws OAuthCodeNotFoundException, JiveInstanceNotFoundException;

}
