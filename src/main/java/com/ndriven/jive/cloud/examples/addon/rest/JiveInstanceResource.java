package com.ndriven.jive.cloud.examples.addon.rest;

import com.ndriven.jive.cloud.examples.addon.model.JiveInstance;
import com.ndriven.jive.cloud.examples.addon.config.JiveInstanceHolder;
import com.ndriven.jive.cloud.examples.addon.oauth.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * Created by caseygum on 2/10/14.
 */

@Path("/jive/instance")
public class JiveInstanceResource extends ResourceBase {
	final Logger logger = LoggerFactory.getLogger(JiveInstanceResource.class);

    @Autowired
	private JiveInstanceHolder jiveInstanceHolder;

    @Autowired
	private OAuthClient oAuthClient;


	@POST
	@Path("/register")
	@Consumes({MediaType.APPLICATION_JSON})
	public void register(JiveInstance jiveInstance) throws IOException, GeneralSecurityException {
		logger.info("Register: " + jiveInstance);
		if (jiveInstance.getCredentials() == null) {
			jiveInstance.setCredentials(new OAuthCredentials());
		}
		jiveInstanceHolder.setJiveInstance(jiveInstance);
		// TODO - validate the Jive Server instance
		setResponseStatus(HttpStatus.NO_CONTENT);
	}

	@POST
	@Path("/unregister")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public void unregister(JiveInstance jiveInstance) throws IOException, GeneralSecurityException {
		logger.info("Unregister: " + jiveInstance);
		jiveInstanceHolder.setJiveInstance(null);
		setResponseStatus(HttpStatus.NO_CONTENT);
	}

	@GET
	@Path("/oauth_callback")
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces(MediaType.TEXT_PLAIN)
	public String oauthCallback(@QueryParam("scope") String scope, @QueryParam("code") String code) throws IOException, GeneralSecurityException, JiveInstanceNotFoundException, OAuthCodeNotFoundException {
		logger.info(String.format("OAuth Callback: code=%s, scope=%s", code, scope));

		JiveInstance jiveInstance = jiveInstanceHolder.getJiveInstance();

		jiveInstance.setCode(code);
		jiveInstance.setScope(scope);
		jiveInstanceHolder.saveInstanceData();

		String errorMessage = "An error occurred authenticating with Jive.";
		try {
			String accessToken  = oAuthClient.getAccessToken();
			return "Authentication succeeded.  You may now close this window.";
		} catch (JiveInstanceNotFoundException e) {
			logger.error(errorMessage, e);
			throw e;
		} catch (OAuthCodeNotFoundException e) {
			logger.error(errorMessage, e);
			throw e;
		}

	}

	@GET
        @Path("/oauth_url")
	@Produces(MediaType.TEXT_PLAIN)
	public String getAuthorizeUrl() throws IOException, GeneralSecurityException, JiveInstanceNotFoundException {
		JiveInstance jiveInstance = jiveInstanceHolder.getJiveInstance();

		if (jiveInstance == null) throw new JiveInstanceNotFoundException();

		String url = String.format(
				"%s/oauth2/authorize?client_id=%s&response_type=code",
				jiveInstance.getJiveUrl(),
				jiveInstance.getClientId()
			);

		return url;
	}


}
