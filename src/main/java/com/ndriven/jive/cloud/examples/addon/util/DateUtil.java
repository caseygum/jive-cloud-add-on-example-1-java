package com.ndriven.jive.cloud.examples.addon.util;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.ISODateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by caseygum on 2/12/14.
 */
public class DateUtil {
	@NotNull
	public static String convertToIsoString(DateTime dt) {
		return ISODateTimeFormat.dateTime().print(dt);
	}
	@NotNull
	public static String convertToIsoString(Date dt) {
		return ISODateTimeFormat.dateTime().print(new DateTime(dt));
	}

	public static DateTime now() {
		return DateTime.now(DateTimeZone.UTC);
	}
}
