package com.ndriven.jive.cloud.examples.addon.rest.exceptions;

import com.ndriven.jive.cloud.examples.addon.oauth.JiveInstanceNotFoundException;
import org.springframework.http.HttpStatus;

import javax.ws.rs.ext.Provider;

/**
 * Created by caseygum on 2/19/14.
 *
 *
 * If the REST API throws a JiveInstanceNotFound exception, this
 * class prepares the error output
 */

@Provider
public class JiveInstanceNotFoundMapper extends ExceptionMapperBase<JiveInstanceNotFoundException>{

	@Override
	protected String getMessage() {
		return "A Jive Instance object could not be found.  " +
			   "This is most likely due to the fact that the " +
			   "add-on has not been installed on a Jive instance.  " +
			   "Jive must first call <context>/jive/instance/register.";
	}

	@Override
	protected int getStatusCode() {
		return HttpStatus.PRECONDITION_FAILED.value();
	}
}
