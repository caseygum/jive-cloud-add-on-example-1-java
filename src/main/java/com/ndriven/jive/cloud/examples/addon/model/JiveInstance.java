package com.ndriven.jive.cloud.examples.addon.model;

import com.ndriven.jive.cloud.examples.addon.oauth.OAuthCredentials;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by caseygum on 2/10/14.
 */
@XmlRootElement
public class JiveInstance {
	protected String clientId;
	protected String clientSecret;
	protected String code;
	protected String scope;
	protected String state;
	protected String jiveUrl;
	protected String jiveSignature;
	protected String jiveSignatureURL;
	protected String tenantId;
	protected String timestamp;
	protected OAuthCredentials oAuthCredentials = new OAuthCredentials();

	public JiveInstance() { }

	public JiveInstance(String clientId, String clientSecret, String code, String scope, String jiveUrl, String jiveSignature, String jiveSignatureURL, String tenantId) {
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.code = code;
		this.scope = scope;
		this.jiveUrl = jiveUrl;
		this.jiveSignature = jiveSignature;
		this.jiveSignatureURL = jiveSignatureURL;
		this.tenantId = tenantId;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getJiveUrl() {
		return jiveUrl;
	}

	public void setJiveUrl(String jiveUrl) {
		this.jiveUrl = jiveUrl;
	}

	public String getJiveSignature() {
		return jiveSignature;
	}

	public void setJiveSignature(String jiveSignature) {
		this.jiveSignature = jiveSignature;
	}

	public String getJiveSignatureURL() {
		return jiveSignatureURL;
	}

	public void setJiveSignatureURL(String jiveSignatureURL) {
		this.jiveSignatureURL = jiveSignatureURL;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	// This is a random value used to insure that the OAuth grant authorization callback
	// actually initiated from a request that the REST API made.
	public String getState() { return state; }

	public void setState(String state) { this.state = state; }

	public OAuthCredentials getCredentials() {
		return oAuthCredentials;
	}

	public void setCredentials(OAuthCredentials oAuthCredentials) {
		this.oAuthCredentials = oAuthCredentials;
	}

    public boolean isValidJiveAuthHeader(String authHeader)
            throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {

        String[] authVars = authHeader.split(" ");
        String authFlag = authVars[0];
        if ("JiveEXTN".equals(authFlag)) {
            String str = "";
            String[] authParams = authVars[1].split("&");
            String signature = null;
            for(String p : authParams) {
                if (p.indexOf("signature") == 0) {
                    signature = p.split("signature=")[1];
                } else {
                    if (str.length() > 0) {
                        str += '&';
                    }
                    str += p;
                }
            }
            if (signature == null) return false;

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            byte[] keyBytes = Base64.decodeBase64(clientSecret);
            SecretKeySpec secret_key = new SecretKeySpec(keyBytes, "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hashBytes = sha256_HMAC.doFinal(str.getBytes());
            String hash = Base64.encodeBase64String(hashBytes);
            return hash.equals(URLDecoder.decode(signature, "UTF-8"));
        }
        return false;

    }

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		JiveInstance that = (JiveInstance) o;

		if (clientId != null ? !clientId.equals(that.clientId) : that.clientId != null) return false;
		if (clientSecret != null ? !clientSecret.equals(that.clientSecret) : that.clientSecret != null) return false;
		if (code != null ? !code.equals(that.code) : that.code != null) return false;
		if (jiveSignature != null ? !jiveSignature.equals(that.jiveSignature) : that.jiveSignature != null)
			return false;
		if (jiveSignatureURL != null ? !jiveSignatureURL.equals(that.jiveSignatureURL) : that.jiveSignatureURL != null)
			return false;
		if (jiveUrl != null ? !jiveUrl.equals(that.jiveUrl) : that.jiveUrl != null) return false;
		if (oAuthCredentials != null ? !oAuthCredentials.equals(that.oAuthCredentials) : that.oAuthCredentials != null)
			return false;
		if (scope != null ? !scope.equals(that.scope) : that.scope != null) return false;
		if (state != null ? !state.equals(that.state) : that.state != null) return false;
		if (tenantId != null ? !tenantId.equals(that.tenantId) : that.tenantId != null) return false;
		if (timestamp != null ? !timestamp.equals(that.timestamp) : that.timestamp != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = clientId != null ? clientId.hashCode() : 0;
		result = 31 * result + (clientSecret != null ? clientSecret.hashCode() : 0);
		result = 31 * result + (code != null ? code.hashCode() : 0);
		result = 31 * result + (scope != null ? scope.hashCode() : 0);
		result = 31 * result + (state != null ? state.hashCode() : 0);
		result = 31 * result + (jiveUrl != null ? jiveUrl.hashCode() : 0);
		result = 31 * result + (jiveSignature != null ? jiveSignature.hashCode() : 0);
		result = 31 * result + (jiveSignatureURL != null ? jiveSignatureURL.hashCode() : 0);
		result = 31 * result + (tenantId != null ? tenantId.hashCode() : 0);
		result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
		result = 31 * result + (oAuthCredentials != null ? oAuthCredentials.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "JiveInstance{" +
				"clientId='" + clientId + '\'' +
				", clientSecret='" + clientSecret + '\'' +
				", code='" + code + '\'' +
				", scope='" + scope + '\'' +
				", state='" + state + '\'' +
				", jiveUrl='" + jiveUrl + '\'' +
				", jiveSignature='" + jiveSignature + '\'' +
				", jiveSignatureURL='" + jiveSignatureURL + '\'' +
				", tenantId='" + tenantId + '\'' +
				", timestamp='" + timestamp + '\'' +
				", oAuthCredentials=" + oAuthCredentials +
				'}';
	}
}
