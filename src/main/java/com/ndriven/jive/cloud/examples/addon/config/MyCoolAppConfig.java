package com.ndriven.jive.cloud.examples.addon.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by caseygum on 2/7/14.
 */
public class MyCoolAppConfig extends ResourceConfig {

	final Logger logger = LoggerFactory.getLogger(MyCoolAppConfig.class);

	public MyCoolAppConfig() {
		logger.info("Configuring my cool app");
	}


}
