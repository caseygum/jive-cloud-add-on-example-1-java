package com.ndriven.jive.cloud.examples.addon.oauth;

import com.google.common.net.HttpHeaders;
import com.ndriven.jive.cloud.examples.addon.config.JiveInstanceHolder;
import com.ndriven.jive.cloud.examples.addon.model.JiveInstance;
import com.ndriven.jive.cloud.examples.addon.rest.RestHelper;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.ws.rs.client.*;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

/**
 * Created by caseygum on 2/15/14.
 */
public class JiveOAuthClientImpl extends RestHelper implements OAuthClient {
	final Logger logger = LoggerFactory.getLogger(JiveOAuthClientImpl.class);

	private JiveInstanceHolder jiveInstanceHolder;

	public JiveOAuthClientImpl(JiveInstanceHolder jiveInstanceHolder) {
		this.jiveInstanceHolder = jiveInstanceHolder;
	}

	@Override
	public String getAccessToken() throws JiveInstanceNotFoundException, OAuthCodeNotFoundException {
		JiveInstance jiveInstance = null;
		try {
			jiveInstance = jiveInstanceHolder.getJiveInstance();
			if (jiveInstance == null) throw new JiveInstanceNotFoundException();
		} catch (Exception e) {
			logger.error("An error occurred getting access token", e);
			if (e instanceof JiveInstanceNotFoundException) throw (JiveInstanceNotFoundException)e;
			throw new JiveInstanceNotFoundException(e);
		}

		if (jiveInstance.getCredentials() == null || !StringUtils.hasText(jiveInstance.getCredentials().getAccessToken())) {
			// we don't have credentials yet, we need to request them using the code
			if (!StringUtils.hasText(jiveInstance.getCode())) throw new OAuthCodeNotFoundException();
			exchangeCodeForTokens(jiveInstance);
		}

		if (jiveInstance.getCredentials().isAccessTokenExpired()) {
			updateAccessToken(jiveInstance);
		}

		return jiveInstance.getCredentials().getAccessToken();
	}

	@Override
	public Invocation.Builder createRequestBuilder(String url) throws OAuthCodeNotFoundException, JiveInstanceNotFoundException {
		Client client = getClient();
		WebTarget target = client.target(url);
		return target.request().header(HttpHeaders.AUTHORIZATION, "Bearer " + getAccessToken());
	}

	@Override
	public Invocation.Builder createMultipartRequestBuilder(String url) throws OAuthCodeNotFoundException, JiveInstanceNotFoundException {
		Client client = getMultipartClient();
		WebTarget target = client.target(url);
		return target.request().header(HttpHeaders.AUTHORIZATION, "Bearer " + getAccessToken());
	}

	private void exchangeCodeForTokens(JiveInstance jiveInstance) {

		logger.info("Requesting access and refresh tokens");
		Client client = getClient();
		client.register(HttpAuthenticationFeature.basic(jiveInstance.getClientId(), jiveInstance.getClientSecret()));
		WebTarget target = client.target(jiveInstance.getJiveUrl() + "/oauth2/token");
		Form form = new Form("grant_type", "authorization_code");
		form.param("code", jiveInstance.getCode());
		form.param("client_id", jiveInstance.getClientId());

		JiveOAuthResponse response = target.request().post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED),JiveOAuthResponse.class);

		logger.info("Access Token : Response : " + response);

		OAuthCredentials credentials = new OAuthCredentials();
		credentials.setAccessToken(response.getAccessToken());
		credentials.setRefreshToken(response.getRefreshToken());
		credentials.setAccessTokenExpiresIn((int) response.getExpiresIn());
		jiveInstance.setCredentials(credentials);

		updateJiveInstance();
	}

	private void updateAccessToken(JiveInstance jiveInstance) {
		logger.info("Exchanging refresh token for access token");
		Client client = getClient();
		client.register(HttpAuthenticationFeature.basic(jiveInstance.getClientId(), jiveInstance.getClientSecret()));
		OAuthCredentials credentials = jiveInstance.getCredentials();
		WebTarget target = client.target(jiveInstance.getJiveUrl() + "/oauth2/token");
		Form form = new Form("grant_type", "refresh_token");
		form.param("refresh_token", credentials.getRefreshToken());
		form.param("client_id", jiveInstance.getClientId());

		JiveOAuthResponse response = target.request().post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED),JiveOAuthResponse.class);

		logger.info("Access Token : Response : " + response);

		credentials.setAccessToken(response.getAccessToken());
		credentials.setAccessTokenExpiresIn((int) response.getExpiresIn());

		updateJiveInstance();
	}

	private void updateJiveInstance() {
		try {
			jiveInstanceHolder.saveInstanceData();
		} catch (Exception ex) {
			logger.error("An error occurred saving Jive Instance data");
		}
	}


}
