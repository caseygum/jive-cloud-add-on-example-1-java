package com.ndriven.jive.cloud.examples.addon.oauth;

/**
 * Created by caseygum on 2/15/14.
 */
public class OAuthCodeNotFoundException extends Exception {
	public OAuthCodeNotFoundException() {
		super("The JiveInstance does not contain an OAuth code.  You must first have Jive grant access permissions to this Add-on.");
	}
}
