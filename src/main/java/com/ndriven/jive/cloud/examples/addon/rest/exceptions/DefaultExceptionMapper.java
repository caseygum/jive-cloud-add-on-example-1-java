package com.ndriven.jive.cloud.examples.addon.rest.exceptions;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotAllowedException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.ext.Provider;

/**
 * Created by caseygum on 2/19/14.
 */
@Provider
public class DefaultExceptionMapper extends ExceptionMapperBase<Exception>{

	@Override
	protected String getMessage() {
		StringBuffer sb = new StringBuffer();
		Throwable ex = getException();
		while (ex != null) {
			sb.append(String.format("%s\n", ex.getMessage()));
			ex = ex.getCause();
		}
		return sb.toString();
	}

	@Override
	protected int getStatusCode() {
        if (getException() instanceof NotFoundException) {
            return 404;
        }
        if (getException() instanceof NotAuthorizedException) {
            return 401;
        }
        if (getException() instanceof ForbiddenException) {
            return 403;
        }
        if (getException() instanceof NotAllowedException) {
            return 405;
        }
		return 500;
	}
}
