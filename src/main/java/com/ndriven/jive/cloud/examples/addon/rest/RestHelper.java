package com.ndriven.jive.cloud.examples.addon.rest;


import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;

/**
 * Created by caseygum on 2/27/14.
 */
public class RestHelper {

	final Logger logger = LoggerFactory.getLogger(RestHelper.class);

	protected Client getClient() {
		Client client = ClientBuilder.newClient();
		client.register(JacksonFeature.class);
		return client;
	}

	protected Client getMultipartClient() {
		Client client = ClientBuilder.newClient();
		client.register(MultiPartFeature.class);
		return client;
	}

	public Invocation.Builder createRequest(String url) {
		Client client = getClient();
		WebTarget target = client.target(url);
		return target.request();
	}

	public Invocation.Builder createMultipartRequest(String url) {
		Client client = getMultipartClient();
		WebTarget target = client.target(url);
		return target.request();
	}
}
