package com.ndriven.jive.cloud.examples.addon.util;


import org.apache.commons.codec.Charsets;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by caseygum on 2/12/14.
 */
public class EncryptionUtil {

	public static byte[] encrypt(String keyString, String value) throws GeneralSecurityException {
		SecretKey key = getKey(keyString);
		byte[] content = value.getBytes(Charsets.UTF_8);

		Cipher cipher = Cipher.getInstance("DES");
		cipher.init(Cipher.ENCRYPT_MODE, key);

		return cipher.doFinal(content);
	}

	public static String encryptAndBase64Encode(String key, String value) throws GeneralSecurityException {
		return new Base64().encodeAsString(encrypt(key, value));
	}

	public static String decrypt(String keyString, byte[] encrypted) throws GeneralSecurityException {
		SecretKey key = getKey(keyString);

		Cipher cipher = Cipher.getInstance("DES");
		cipher.init(Cipher.DECRYPT_MODE, key);

		return new String(cipher.doFinal(encrypted), Charsets.UTF_8);
	}

	public static String base64DecodeAndDecrypt(String key, String base64EncodedEncrypted) throws GeneralSecurityException {
		return decrypt(key, new Base64().decode(base64EncodedEncrypted));
	}

	private static SecretKey getKey(String keyString) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException {
		DESKeySpec keySpec = new DESKeySpec(keyString.getBytes(Charsets.UTF_8));
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
		return keyFactory.generateSecret(keySpec);
	}

}
