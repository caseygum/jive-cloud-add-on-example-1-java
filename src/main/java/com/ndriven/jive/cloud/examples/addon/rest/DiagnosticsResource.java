package com.ndriven.jive.cloud.examples.addon.rest;

import com.ndriven.jive.cloud.examples.addon.config.JiveInstanceHolder;
import com.ndriven.jive.cloud.examples.addon.model.JiveInstance;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * Created by caseygum on 4/21/14.
 */

@Path("/diagnostics")
public class DiagnosticsResource {

    @Autowired @Qualifier("diagnosticsKey")
    private String diagnosticsKey;

    @Autowired @Qualifier("appProfile")
    private String appProfile;

    @Autowired @Qualifier("appVersion")
    private String appVersion;

    @Autowired @Qualifier("appContext")
    private String appContext;

    @Autowired @Qualifier("appUrl")
    private String appUrl;

    @Autowired @Qualifier("serviceUrl")
    private String serviceUrl;

    @Autowired @Qualifier("appConfigDir")
    private String configDir;

    @Autowired @Qualifier("jiveInstanceHolder")
    private JiveInstanceHolder instanceHolder;

    @Context
    private UriInfo context;
    @Context private HttpServletRequest request;
    @Context private HttpServletResponse response;

    @GET
    @Path("/app-details")
    @Produces({"application/json;qs=1", "application/xml;qs=.5"})
    public String getVersion() {
        validateRequest();
        JSONObject details = new JSONObject();
        details.put("profile", appProfile);
        details.put("version", appVersion);
        details.put("context", appContext);
        details.put("appUrl", appUrl);
        details.put("serviceUrl", serviceUrl);
        details.put("configDir", configDir);

        return details.toString();
    }

    @GET
    @Path("/jive-instance-details")
    @Produces({"application/json;qs=1", "application/xml;qs=.5"})
    public String getJiveInstanceDetails() throws IOException, GeneralSecurityException {
        validateRequest();

        JiveInstance instance = instanceHolder.getJiveInstance();

        if (instance == null) {
            JSONObject responseObject = new JSONObject();
            responseObject.put("error", "The JiveInstance object is null.  The add-on has not been installed, or an error occurred during installation/registration.");
            return responseObject.toString();
        } else {
            ObjectMapper mapper = new ObjectMapper();
            String instanceAsJson = mapper.writeValueAsString(instanceHolder.getJiveInstance());

            return instanceAsJson;
        }
    }

    private void validateRequest() {
        if (diagnosticsKey != null && diagnosticsKey.equals(getRequest().getHeader("X-Diagnostics-Key"))) {
            return;
        }

        throw new NotAuthorizedException("The Authorization header in the request could not be validated.", "X-Diagnostics-Key", (Object[]) null);
    }

    protected UriInfo getContext() {
        return context;
    }

    protected HttpServletRequest getRequest() {
        return request;
    }

    protected HttpServletResponse getResponse() {
        return response;
    }

}
