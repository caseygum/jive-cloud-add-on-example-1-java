package com.ndriven.jive.cloud.examples.addon.model;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by caseygum on 2/10/14.
 */
@XmlRootElement
public class User {
	private Long id;
	private String userName;
	private String email;

	public User(@NotNull Long id, @NotNull String userName, @NotNull String email) {
		this.id = id;
		this.userName = userName;
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public String getUserName() {
		return userName;
	}

	public String getEmail() {
		return email;
	}
}
