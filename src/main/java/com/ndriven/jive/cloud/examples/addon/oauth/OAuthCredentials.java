/*
 *
 *  * Copyright 2013 Jive Software
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.ndriven.jive.cloud.examples.addon.oauth;

import static com.ndriven.jive.cloud.examples.addon.util.DateUtil.*;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Created by rrutan on 1/30/14.
 */
public class OAuthCredentials {
    private static final Logger log = LoggerFactory.getLogger(OAuthCredentials.class);

    private static final String INVALID = "invalid";
    private static final String OK = "ok";

    private String accessToken = null;

    private String refreshToken = null;

	private DateTime expiresOn = new DateTime(new Date(0));

    public OAuthCredentials() { }

    public OAuthCredentials(String accessToken, String refreshToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

	public void setAccessTokenExpiresIn(int expiresIn) {
		expiresOn = now().plusSeconds(expiresIn - 15); // knock off 15 seconds for good measure
	}

	public Date getExpiresOn() {
		return expiresOn.toDate();
	}

	public void setExpiresOn(Date date) {
		expiresOn = new DateTime(date);
	}

	@JsonIgnore
	public boolean isAccessTokenExpired() {
		return now().isAfter(expiresOn);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		OAuthCredentials that = (OAuthCredentials) o;

		if (accessToken != null ? !accessToken.equals(that.accessToken) : that.accessToken != null) return false;
		if (expiresOn != null ? !expiresOn.equals(that.expiresOn) : that.expiresOn != null) return false;
		if (refreshToken != null ? !refreshToken.equals(that.refreshToken) : that.refreshToken != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = accessToken != null ? accessToken.hashCode() : 0;
		result = 31 * result + (refreshToken != null ? refreshToken.hashCode() : 0);
		result = 31 * result + (expiresOn != null ? expiresOn.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "OAuthCredentials{" +
				"accessToken='" + accessToken + '\'' +
				", refreshToken='" + refreshToken + '\'' +
				", expiresOn=" + expiresOn +
				'}';
	}
}
