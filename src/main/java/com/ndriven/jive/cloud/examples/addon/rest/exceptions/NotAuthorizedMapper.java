package com.ndriven.jive.cloud.examples.addon.rest.exceptions;

import org.springframework.http.HttpStatus;

import javax.ws.rs.NotAuthorizedException;

/**
 * Created by caseygum on 2/19/14.
 */
public class NotAuthorizedMapper extends ExceptionMapperBase<NotAuthorizedException> {
	@Override
	protected String getMessage() {
        return getException().getMessage();
	}

	@Override
	protected int getStatusCode() {
		return HttpStatus.UNAUTHORIZED.value();
	}
}
