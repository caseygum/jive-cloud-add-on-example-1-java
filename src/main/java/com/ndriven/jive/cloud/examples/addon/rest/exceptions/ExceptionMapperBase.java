package com.ndriven.jive.cloud.examples.addon.rest.exceptions;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.json.JSONObject;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Created by caseygum on 2/19/14.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class ExceptionMapperBase <E extends Throwable> implements ExceptionMapper<E> {
	private E exception;

	protected abstract String getMessage();
	protected abstract int getStatusCode();

	public E getException() {
		return exception;
	}

	protected String getContentType() {
		return "application/json";
	}

	@Override
	public Response toResponse(E exception) {
		this.exception = exception;


        JSONObject body = new JSONObject();
        JSONObject error = new JSONObject();
        body.put("status", getStatusCode());
        error.put("code", getStatusCode());
        String message = getException().getMessage();
        error.put("message", message);
        body.put("error", error);

		Response response =  Response
				.status(getStatusCode())
				.type(getContentType())
				.entity(body.toString())
				.build();

		return response;

	}
}
